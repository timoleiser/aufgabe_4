import React, {useState} from "react";

function Aufgabe_3() {
    const data = [
        { title: 'red', red: 255, green: 0, blue: 0 },
        { title: 'green', red: 0, green: 255, blue: 0},
        { title: 'blue', red: 0, green: 0, blue: 255},
    ]
    const [colorsStrunkt, setColors] = useState(data);

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    return (
        <div>
            <div>
                <ul>
                    { colorsStrunkt.map(color => <li style={{color: rgbToHex(color.red,color.green,color.blue)}}>{color.title+" ("+color.red+","+color.green+","+color.blue+"), "+rgbToHex(color.red,color.green,color.blue)}</li>) }
                </ul>
            </div>
        </div>
    );
}

export default Aufgabe_3;
