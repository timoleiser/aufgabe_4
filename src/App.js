import './App.css';
import React, {useState} from "react";
import Aufgabe_1 from "./Aufgabe_1";
import Aufgabe_2 from "./Aufgabe_2";
import Aufgabe_3 from "./Aufgabe_3";
import Aufgabe_4 from "./Aufgabe_4";

function App() {
  return (
   <div>
      <div>
        <h1>Aufgabe 1</h1>
        <Aufgabe_1/>
        <h1>Aufgabe 2</h1>
        <Aufgabe_2/>
        <h1>Aufgabe 3</h1>
        <Aufgabe_3/>
        <h1>Aufgabe 4</h1>
        <Aufgabe_4/>
      </div>
   </div>
    );
}

export default App;
