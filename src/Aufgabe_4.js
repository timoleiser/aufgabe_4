import React, {useState} from "react";

function Aufgabe_4() {
    const data = [
        { title: 'red', red: 255, green: 0, blue: 0 },
        { title: 'green', red: 0, green: 255, blue: 0},
        { title: 'blue', red: 0, green: 0, blue: 255},
        { title: "random 1", red: Math.floor(Math.random()*255), blue: Math.floor(Math.random()*255), green: Math.floor(Math.random()*255)}

    ]
    const [data2,setData2] = useState([]);

    const [text, setText] = useState("");

    const [colorsStrunkt, setColors] = useState(data);

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function randomColor(event){
        const getRandomValue = () => {
            const {floor, random} = Math;
            return floor(random() * 255);
        }
        setData2([...data2, {title: text, red: getRandomValue(), green: getRandomValue()
        , blue: getRandomValue()}]);
    }
    return (
        <div>
            <div className={"aktion"}>
                <input type="text" value={text} onChange={(e) => setText(e.target.value)}/>
                <input type="button" value="Submit" onClick={randomColor}/>
            </div>
            <div>
                <ul>{data2.map(({title, red, green, blue}, index) =>
                    <li style={{color: `rgb(${red}, ${green}, ${blue})`}}>{title + "(" + red + ", " + green + ", " + blue +")"}</li>)}
                </ul>
            </div>

        </div>
    );
}

export default Aufgabe_4;
