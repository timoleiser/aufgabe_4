import React, {useState} from "react";

function Aufgabe_1() {
    const colors = ['red', 'green', 'blue']
    return (
        <div>
            <div>
                <ul>
                    { colors.map(color => <li>{color}</li>) }
                </ul>
            </div>
        </div>
    );
}

export default Aufgabe_1;
